package main

import "fmt"

func maskLinks(input string) string {
    inputBytes := []byte(input)
    for i := 0; i < len(inputBytes)-6; i++ {
        if string(inputBytes[i:i+7]) == "http://" {
            j := i + 7
            for j < len(inputBytes) && inputBytes[j] != ' ' {
                inputBytes[j] = '*'
                j++
            }
            i = j
        }
    }
    maskedString := string(inputBytes)
    return maskedString
}

func main(){
	input := "Here's my spammy page: http://hehefouls.netHAHAHA see you"
	output := maskLinks(input)
	fmt.Println(output)
}